<?php

use PHPUnit\Framework\TestCase;

class CrudTest extends TestCase
{
    private $bdd;

    public function setUp(): void
    {
        $this->bdd = new PDO("sqlite:db.sqlite");
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function testCreate()
    {
        $query = "INSERT INTO contacte (nom, prenom, adresse) VALUES (?, ?, ?)";
        $stmt = $this->bdd->prepare($query);
        $nom = "NouveauNom5";
        $prenom = "NouveauPrenom";
        $adresse = "NouvelleAdresse";
        $stmt->execute([$nom, $prenom, $adresse]);
        $this->assertEquals(1, $stmt->rowCount());
    }

    public function testRead()
    {
        $query = "SELECT * FROM contacte";
        $stmt = $this->bdd->query($query);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $this->assertGreaterThan(0, count($result));
    }

    public function testUpdate()
    {
        $query = "UPDATE contacte SET nom = ?, prenom = ? WHERE id = ?";
        $stmt = $this->bdd->prepare($query);
        $nom = "NouveauNomUpdated";
        $prenom = "NouveauPrenomUpdated";
        $id = 1;
        $stmt->execute([$nom, $prenom, $id]);
        $this->assertEquals(1, $stmt->rowCount());
    }

    public function testDeleteLastRecord()
    {
        $query = "DELETE FROM contacte WHERE id = (SELECT MAX(id) FROM contacte)";
        $stmt = $this->bdd->prepare($query);
        $stmt->execute();
        $this->assertEquals(1, $stmt->rowCount());
    }
}
