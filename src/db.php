<?php

namespace MaCalculatrice;

use PDO;
use PDOException;

class DB
{
    private $bdd;

    public function __construct()
    {
        $this->bdd = new PDO("sqlite:db.sqlite");
        $this->bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    public function insertQuery(string $nom, string $prenom, string $adresse)
    {
        try {
            $query = "INSERT INTO contacte (nom, prenom, adresse) VALUES (?, ?, ?)";
            $stmt = $this->bdd->prepare($query);
            $stmt->execute([$nom, $prenom, $adresse]);
            echo "Données insérées avec succès.";
        } catch (PDOException $e) {
            echo "Erreur lors de l'insertion : " . $e->getMessage();
        }
    }
}
